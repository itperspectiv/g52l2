//
//  Math.swift
//  G52L2
//
//  Created by Ivan Vasilevich on 2/13/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class Math: NSObject {
    
    static func playWithWariables() {
        let pi = 3.14159
//        pi = 3 + 2
        print("pi number = \(pi)")
        var age = 30
        print("age =", age, "years old")
        age = 32 + 3
        print("age =", age, "years old")
        // + - * / %
        // + -
        //a = 15.5 % 7
        age = -Int(pi) + 32
        age = age + 2
        age += 2
//        let apples = 3
//        var a = pi + apples
    }
    
    static func randomNumber() {
        let randomNumber = arc4random()%3
        // > < >= <= == !=
        if randomNumber == 0 {
            print("tiger", randomNumber)
            let b = 10
            print(b)
//            return
        }
        else if randomNumber == 1 {
            print("eurasia")
        }
        else {
            print("casta")
        }
//        print(b)
        
        switch randomNumber {
        case 0:
            print("tiger", randomNumber)
        case 1:
            print("eurasia")
        default:
            print("casta")
        }
    }
    
    static func lifeIsGame() {
        var money = arc4random()%100
        let age = arc4random()%65
        print("man had \(money)k$")
        money = arc4random()%100
        print("man had \(money)k$")
        
        let isOld = age <= 30
        // || or / && and
        if money >= 50 || !isOld {
            print("success")
        }
        else {
            print("fail")
        }
    }
    
    static func loopExample() {
        
        for counter in 0..<10 {
            for i in 0..<3 {
                print("hello#", counter, i)
            }
        }
        
    }
    
    static func squareOfFive() {
        let number = 5
        let result = number * number
        print("square of \(number) = \(result)")
    }
    
    static func squareOfNumber(value: Double) -> Double {
        let number = value
        let result = number * number
        print("square of \(number) = \(result)")
        return result
    }
    
    static func methWithMultipleArghs(value1: Int, value2: Bool) {
        print("val1", value1, "val2", value2)
    }
    
    static func giveMeFive() -> Int {
        let numb = 5
        return numb
    }

}
