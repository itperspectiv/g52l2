//
//  ViewController.swift
//  G52L2
//
//  Created by Ivan Vasilevich on 2/13/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        print("hello privet")
        Math.playWithWariables()
        Math.randomNumber()
        
        //true life game
        Math.lifeIsGame()
        //loop grid
        Math.loopExample()
        
        Math.squareOfFive()
        let sqSum = Math.squareOfNumber(value: 3) + Math.squareOfNumber(value: 4)
        print(sqSum)
        Math.methWithMultipleArghs(value1: 2, value2: true)
        let a = 5
        Math.methWithMultipleArghs(value1: a, value2: !true)
        print(Math.giveMeFive() + 5)
    }




}

